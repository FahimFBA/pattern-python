# Pattern Printing 

## Patterns with the Video Tutorial

#### [⭐ Check the full playlist](https://www.youtube.com/playlist?list=PL7ZCWbO2Dbl5n9oOiG0V3ZXzt-6W1pOnH)

### 🎖️ Pattern 1

```
*
* *
* * *
* * * *
* * * * *
```
[Check the Video tutorial](https://youtu.be/apq1sHtj9Fk)

### 🎖️ Pattern 2

```
* * * * *
* * * * *
* * * * *
* * * * *
* * * * *
```
[Check the Video tutorial]()

### 🎖️ Pattern 2 - Understanding the `range` function

[Check the Video tutorial]()
